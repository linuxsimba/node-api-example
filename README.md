This is a simple hello world app using node.

Wanted to understand the core components to making a decent hello demo.
All the parts are listed in package.json.

First install node. I'd recommend using installing node using NVM

```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.28.0/install.sh |
bash
```
Log out of terminal

Log back into the terminal

```
nvm install 4.1.1
npm install bower -g
npm install express -g
npm install coffee-script -g
```

git clone this project


Run ``npm install `` within the repo directory to install all the bits to make it
work.

Run ``bower install `` from within the repo directory to install all client
side javascript dependencies.

Then run ``node server.js``. It starts a webservice on port 3000.


## Directory Structure.. Pretty simple
```
.
├── app.coffee
├── assets
│   ├── css
│   │   └── site.styl
│   └── js
│       └── site.coffee
├── bower.json
├── package.json
├── README.md
├── server.js
└── views
    └── index.jade
```