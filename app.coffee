# core  webservice framework.
express = require 'express'

# this is part of node.js handling and transforming file paths.
app = express()

# add support for req.body from POST and PUT requests
bodyParser = require('body-parser')
app.use(bodyParser.json())

# use pug as the templating engine.
# so all files in ./views directory end in .styl
app.set 'view engine', 'pug'

# This is like Rails asset mgmt thing.
app.use require('connect-assets') (
      paths: ["assets/js", "assets/css", "bower_components"]
)

# Create the home page. renders index.jade and then runs
# Jquery code in site.coffee. changes background color as defined in site.styl
app.get '/', (req, res, next) ->
  res.render 'index', title: 'Hello World with Jquery Enabled'

# Middleware defining how to check for 404 Error
require('./middleware/404')(app)


server = app.listen '3000'

if app.get('env') is 'development'
    console.log("Start Express server at #{config.express.dev.port}")
    server = app.listen config.express.dev.port
    app.chai = require 'chai'
    app.sinon = require 'sinon'
else
    console.log("Start Express server at #{config.express.production.port}")
    server = app.listen config.express.production.port

# Important when creating the models, controller, and tests
# you call ``require ('./app')``
module.exports = app
